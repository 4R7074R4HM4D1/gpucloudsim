/**
 * 
 */
package org.cloudbus.cloudsim.gpu;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.cloudbus.cloudsim.Cloudlet;
import org.cloudbus.cloudsim.DatacenterBroker;
import org.cloudbus.cloudsim.DatacenterCharacteristics;
import org.cloudbus.cloudsim.Log;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.core.CloudSim;
import org.cloudbus.cloudsim.core.CloudSimTags;
import org.cloudbus.cloudsim.core.SimEvent;
import org.cloudbus.cloudsim.core.predicates.PredicateType;
import org.cloudbus.cloudsim.gpu.core.GpuCloudSimTags;
import org.cloudbus.cloudsim.lists.VmList;

/**
 * {@link GpuDatacenterBroker} extends {@link DatacenterBroker} to add support
 * for GpuCloudlets. Each {@link GpuCloudlet} must have a {@link GpuVm}
 * associated with it. If a {@link GpuVm} fails to find a {@link GpuHost} to
 * run, it is immediately rescheduled to try other options, although they may
 * have been tried before.
 * 
 * @author Ahmad Siavashi
 * 
 */
public class GpuDatacenterBroker extends DatacenterBroker {

	/** A structure to maintain VM-GpuCloudlet mapping */
	private HashMap<String, List<GpuCloudlet>> VmGpuCloudletMap;

	/** The number of submitted gpuCloudlets in each vm. */
	private HashMap<String, Integer> vmGpuCloudletsSubmitted;

	/**
	 * @see DatacenterBroker
	 */
	public GpuDatacenterBroker(String name) throws Exception {
		super(name);
		setGpuVmCloudletMap(new HashMap<String, List<GpuCloudlet>>());
		setVmGpuCloudletsSubmitted(new HashMap<String, Integer>());
	}

	@Override
	protected void finishExecution() {
		for (Integer datacenterId : getDatacenterIdsList()) {
			CloudSim.cancelAll(datacenterId.intValue(), new PredicateType(GpuCloudSimTags.GPU_VM_DATACENTER_EVENT));
		}
		super.finishExecution();
	}

	@Override
	protected void createVmsInDatacenter(int datacenterId) {
		// send as much vms as possible for this datacenter before trying the
		// next one
		int requestedVms = 0;
		for (Vm vm : getVmList()) {
			if (!getVmsToDatacentersMap().containsKey(vm.getId()) && !getVmsCreatedList().contains(vm)) {
				getVmsToDatacentersMap().put(vm.getId(), datacenterId);
				sendNow(datacenterId, CloudSimTags.VM_CREATE_ACK, vm);
				requestedVms++;
			}
		}
		getDatacenterRequestedIdsList().add(datacenterId);

		setVmsRequested(requestedVms);
		setVmsAcks(0);
	}
	
	@Override
	protected void processResourceCharacteristics(SimEvent ev) {
		DatacenterCharacteristics characteristics = (DatacenterCharacteristics) ev.getData();
		getDatacenterCharacteristicsList().put(characteristics.getId(), characteristics);

		if (getDatacenterCharacteristicsList().size() == getDatacenterIdsList().size()) {
			setDatacenterRequestedIdsList(new ArrayList<Integer>());
			createVmsInDatacenter(getDatacenterIdsList().get(0));
		}
	}

	@Override
	protected void processVmCreate(SimEvent ev) {
		int[] data = (int[]) ev.getData();
		int datacenterId = data[0];
		int vmId = data[1];
		int result = data[2];

		if (result == CloudSimTags.TRUE) {
			getVmsToDatacentersMap().put(vmId, datacenterId);
			getVmsCreatedList().add(VmList.getById(getVmList(), vmId));
			setVmsAcks(getVmsAcks() + 1);
			Log.printConcatLine(CloudSim.clock(), ": ", getName(), ": VM #", vmId, " has been created in Datacenter #",
					datacenterId, ", Host #", VmList.getById(getVmsCreatedList(), vmId).getHost().getId());
			// VM has been created successfully, submit its cloudlets now.
			String vmUid = Vm.getUid(getId(), vmId);
			List<GpuCloudlet> vmCloudlets = getVmGpuCloudletMap().get(vmUid);
			for (GpuCloudlet cloudlet : vmCloudlets) {
				submitGpuCloudlet(cloudlet);
			}
			getVmGpuCloudletsSubmitted().put(vmUid, vmCloudlets.size());
			// remove submitted cloudlets from queue
			getCloudletList().removeAll(vmCloudlets);
			getVmGpuCloudletMap().get(vmUid).removeAll(vmCloudlets);
			getVmGpuCloudletMap().remove(vmUid);
			// If this was the last cloudlet, then there is no need to hold
			// vm-gpucloudlet mapping anymore.
			if (getCloudletList().size() == 0) {
				getVmGpuCloudletMap().clear();
			}
		} else {
			Log.printConcatLine(CloudSim.clock(), ": ", getName(), ": Creation of VM #", vmId,
					" failed in Datacenter #", datacenterId);
			// Create the VM in another Datacenter and circularly repeat.
			int nextDatacenterId = getDatacenterIdsList()
					.get((getDatacenterIdsList().indexOf(datacenterId) + 1) % getDatacenterIdsList().size());
			if (!getDatacenterRequestedIdsList().contains(nextDatacenterId)) {
				getDatacenterRequestedIdsList().add(nextDatacenterId);
			}
			getVmsToDatacentersMap().replace(vmId, nextDatacenterId);
			sendNow(nextDatacenterId, CloudSimTags.VM_CREATE_ACK, VmList.getById(getVmList(), vmId));
		}
	}

	protected void processVmDestroy(SimEvent ev) {
		int[] data = (int[]) ev.getData();
		int datacenterId = data[0];
		int vmId = data[1];
		int result = data[2];

		if (result == CloudSimTags.TRUE) {
			Log.printLine(CloudSim.clock() + ": VM #" + vmId + " destroyed in Datacenter #" + datacenterId);
			setVmsDestroyed(getVmsDestroyed() + 1);
			getVmGpuCloudletsSubmitted().remove(Vm.getUid(getId(), vmId));
		} else {
			Log.printLine(CloudSim.clock() + ": Failed to destroy VM #" + vmId + " in Datacenter #" + datacenterId);
		}
	}

	@Override
	protected void processCloudletReturn(SimEvent ev) {
		Cloudlet cloudlet = (Cloudlet) ev.getData();
		getCloudletReceivedList().add(cloudlet);
		Log.printConcatLine(CloudSim.clock(), ": ", getName(), ": Cloudlet ", cloudlet.getCloudletId(), " received");
		GpuVm cloudletVm = (GpuVm) VmList.getByIdAndUserId(getVmList(), cloudlet.getVmId(), getId());
		getVmGpuCloudletsSubmitted().replace(cloudletVm.getUid(),
				getVmGpuCloudletsSubmitted().get(cloudletVm.getUid()) - 1);
		cloudletsSubmitted--;
		if (getVmGpuCloudletsSubmitted().get(cloudletVm.getUid()) == 0) {
			sendNow(getVmsToDatacentersMap().get(cloudlet.getVmId()), CloudSimTags.VM_DESTROY_ACK, cloudletVm);
			getVmsCreatedList().remove(cloudletVm);
		}
		// all cloudlets executed
		if (getCloudletList().isEmpty() && cloudletsSubmitted == 0) {
			Log.printConcatLine(CloudSim.clock(), ": ", getName(), ": All Jobs executed. Finishing...");
			clearDatacenters();
			finishExecution();
		}
	}

	@Override
	protected void processOtherEvent(SimEvent ev) {
		switch (ev.getTag()) {
		// VM Destroy answer
		case CloudSimTags.VM_DESTROY_ACK:
			processVmDestroy(ev);
			break;
		default:
			super.processOtherEvent(ev);
			break;
		}
	}

	protected void submitGpuCloudlet(GpuCloudlet gpuCloudlet) {
		sendNow(getVmsToDatacentersMap().get(gpuCloudlet.getVmId()), CloudSimTags.CLOUDLET_SUBMIT, gpuCloudlet);
		getCloudletSubmittedList().add(gpuCloudlet);
		cloudletsSubmitted++;
	}

	@Override
	public void submitCloudletList(List<? extends Cloudlet> list) {
		getCloudletList().addAll(list);
		if (getVmList().size() == 0) {
			throw new IllegalArgumentException("no vm submitted.");
		}
		for (Cloudlet cloudlet : getCloudletList()) {
			if (cloudlet.getVmId() < 0) {
				throw new IllegalArgumentException("cloudlet (#" + cloudlet.getCloudletId() + ") has no VM.");
			}
			Vm vm = VmList.getById(getVmList(), cloudlet.getVmId());
			if (vm == null) {
				throw new IllegalArgumentException("no such vm (Id #" + cloudlet.getVmId() + ") exists for cloudlet (#"
						+ cloudlet.getCloudletId() + ")");
			}
			if (!getVmGpuCloudletMap().containsKey(vm.getUid())) {
				getVmGpuCloudletMap().put(vm.getUid(), new ArrayList<GpuCloudlet>());
			}
			getVmGpuCloudletMap().get(vm.getUid()).add((GpuCloudlet) cloudlet);
		}
	}

	/**
	 * @return the vmGpuCloudletsSubmitted
	 */
	protected HashMap<String, Integer> getVmGpuCloudletsSubmitted() {
		return vmGpuCloudletsSubmitted;
	}

	/**
	 * @param vmGpuCloudletsSubmitted
	 *            the vmGpuCloudletsSubmitted to set
	 */
	protected void setVmGpuCloudletsSubmitted(HashMap<String, Integer> vmGpuCloudletsSubmitted) {
		this.vmGpuCloudletsSubmitted = vmGpuCloudletsSubmitted;
	}

	/**
	 * @return the vmGpuCloudletMap
	 */
	protected HashMap<String, List<GpuCloudlet>> getVmGpuCloudletMap() {
		return VmGpuCloudletMap;
	}

	/**
	 * @param vmGpuCloudletMap
	 *            the vmGpuCloudletMap to set
	 */
	protected void setGpuVmCloudletMap(HashMap<String, List<GpuCloudlet>> vmGpuCloudletMap) {
		VmGpuCloudletMap = vmGpuCloudletMap;
	}
}
