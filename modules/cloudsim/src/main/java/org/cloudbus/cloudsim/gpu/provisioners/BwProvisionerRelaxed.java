package org.cloudbus.cloudsim.gpu.provisioners;

import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.provisioners.BwProvisionerSimple;

/**
 * BwProvisionerRelaxed is an extension of {@link BwProvisionerSimple} that is
 * used to relax the possible limitation of bw when allocating GpuVms to
 * GpuHosts. It always allocates bw for the given GpuVms if the requested bw is
 * lower than host's peak bw.
 * 
 * @author Ahmad Siavashi
 */
public class BwProvisionerRelaxed extends BwProvisionerSimple {
	/**
	 * Instantiates a new bw provisioner relaxed.
	 * 
	 * @param bw
	 *            The total bw capacity from the host that the provisioner can
	 *            allocate to VMs.
	 */
	public BwProvisionerRelaxed(long bw) {
		super(bw);
	}

	@Override
	public boolean allocateBwForVm(Vm vm, long bw) {
		deallocateBwForVm(vm);

		if (getAvailableBw() >= bw) {
			getBwTable().put(vm.getUid(), bw);
			vm.setCurrentAllocatedBw(getAllocatedBwForVm(vm));
			return true;
		}

		vm.setCurrentAllocatedBw(getAllocatedBwForVm(vm));
		return false;
	}

	@Override
	public void deallocateBwForVm(Vm vm) {
		if (getBwTable().containsKey(vm.getUid())) {
			getBwTable().remove(vm.getUid());
			vm.setCurrentAllocatedBw(0);
		}
	}
}
