/**
 * 
 */
package org.cloudbus.cloudsim.gpu.performance;

import java.util.ArrayList;
import java.util.List;

import org.cloudbus.cloudsim.Pe;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.VmScheduler;
import org.cloudbus.cloudsim.gpu.GpuHost;
import org.cloudbus.cloudsim.gpu.GpuVm;
import org.cloudbus.cloudsim.gpu.Vgpu;
import org.cloudbus.cloudsim.gpu.allocation.VideoCardAllocationPolicy;
import org.cloudbus.cloudsim.provisioners.BwProvisioner;
import org.cloudbus.cloudsim.provisioners.RamProvisioner;

/**
 * {@link PerformanceGpuHost} extends {@link GpuHost} to add support for
 * schedulers that implement {@link PerformanceScheduler PerformanceScheduler} interface.
 * 
 * @author Ahmad Siavashi
 * 
 */
public class PerformanceGpuHost extends GpuHost {

	/**
	 * @see org.cloudbus.cloudsim.gpu.GpuHost#GpuHost GpuHost
	 */
	public PerformanceGpuHost(int id, int type, RamProvisioner ramProvisioner, BwProvisioner bwProvisioner,
			long storage, List<? extends Pe> peList, VmScheduler vmScheduler,
			VideoCardAllocationPolicy videoCardAllocationPolicy) {
		super(id, type, ramProvisioner, bwProvisioner, storage, peList, vmScheduler, videoCardAllocationPolicy);
	}
	
	/**
	 * @see org.cloudbus.cloudsim.gpu.GpuHost#GpuHost GpuHost
	 */
	public PerformanceGpuHost(int id, int type, RamProvisioner ramProvisioner, BwProvisioner bwProvisioner,
			long storage, List<? extends Pe> peList, VmScheduler vmScheduler) {
		super(id, type, ramProvisioner, bwProvisioner, storage, peList, vmScheduler);
	}

	@Override
	public double updateVmsProcessing(double currentTime) {
		// To collect Vm that are probably sharing a resource
		List<Vgpu> runningVgpus = new ArrayList<Vgpu>();
		// Collect running gpu vms
		for (Vm vm : getVmList()) {
			GpuVm gpuVm = (GpuVm) vm;
			Vgpu vgpu = gpuVm.getVgpu();
			if (vgpu != null && vgpu.getGpuTaskScheduler().runningTasks() > 0) {
				runningVgpus.add(vgpu);
			}
		}
		double smallerTime = Double.MAX_VALUE;
		for (Vm vm : getVmList()) {
			GpuVm gpuVm = (GpuVm) vm;
			double time = gpuVm.updateVmProcessing(currentTime, getVmScheduler().getAllocatedMipsForVm(gpuVm));
			if (gpuVm.getVgpu() != null) {
				@SuppressWarnings("unchecked")
				PerformanceScheduler<Vgpu> vgpuScheduler = (PerformanceScheduler<Vgpu>) getVideoCardAllocationPolicy()
						.getVgpuVideoCardMap().get(gpuVm.getVgpu()).getVgpuScheduler();
				double deviceTime = gpuVm.getVgpu().updateTaskProcessing(currentTime,
						vgpuScheduler.getAvailableMips(gpuVm.getVgpu(), runningVgpus));
				if (gpuVm.getVgpu().getGpuTaskScheduler().runningTasks() > 0) {
					if (time == 0) {
						time = deviceTime;
					} else {
						time = ((deviceTime < time) ? deviceTime : time);
					}
				}
			}
			if (time > 0.0 && time < smallerTime) {
				smallerTime = time;
			}
		}

		return smallerTime;
	}

}
