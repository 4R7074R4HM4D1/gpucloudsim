package org.cloudbus.cloudsim.gpu.core;

/**
 * Contains Gpu-related events in the simulator.
 * {@link org.cloudbus.cloudsim.core.CloudSimTags} cannot be extended due to its
 * private constructor.
 * 
 * @author Ahmad Siavashi
 * 
 */
public class GpuCloudSimTags {

	/**
	 * Denotes an event to submit a GpuTask for execution.
	 */
	public final static int GPU_TASK_SUBMIT = 49;

	/**
	 * Denotes an internal event in the GpuDatacenter. Updating the progress of
	 * execution and memory transfers are triggered by this event.
	 */
	public final static int GPU_VM_DATACENTER_EVENT = 50;

	/**
	 * Denote an event to evaluate the power consumption of a
	 * {@link org.cloudbus.cloudsim.gpu.power.PowerGpuDatacenter
	 * PowerGpuDatacenter}.
	 */
	public final static int GPU_VM_DATACENTER_POWER_EVENT = 51;

}
